# File: myautomount.spec
# Location: myautomount source
# Author: bgstack15
# Startdate: 2020-09-29
# Title: Rpm Spec for Myautomount Package
# Purpose: Provide build instructions for Fedora rpm for package
# History:
# Usage:
# Reference:
#    bgscripts.spec
#    https://src.fedoraproject.org/rpms/crypto-policies/blob/master/f/crypto-policies.spec
# Improve:
# Documentation:
# Dependencies:

%global use_local 1
%global use_commit 0
%define date %(date +%%Y%%m%%d)
%if 0%{?use_commit}
%global git_commit 0d5f8b7657527eab8380f5aa81ecd072714cf13c
%{?git_commit:%global git_commit_hash %(c=%{git_commit}; echo ${c:0:7})}
%else
%global git_commit 0.0.5
%endif
%global releasenum 1

%define debug_package %{nil}
%global _python_bytecompile_errors_terminate_build 0

Summary:	   tray icon for removable media
Name:       myautomount
%if 0%{?use_local}
Version:    %{date}
Release:    %{releasenum}
Source:     %{name}.tar.gz
%else
%if 0{%?use_commit}
Version:	   %{date}
Release:    %{releasenum}.git%{git_commit_hash}
%else
Version:	   %{git_commit}
Release:    %{releasenum}
%endif
Source:     https://gitlab.com/bgstack15/%{name}/-/archive/%{git_commit}/%{name}-%{git_commit}.tar.gz
%endif
License:	   CC BY-SA 4.0 and BSD-2-Clause
Group:      Applications/System
URL:        https://bgstack15.wordpress.com/
Packager:   B. Stack <bgstack15@gmail.com>
Requires:   autofs
Requires:   sudo
Requires:   python3-inotify
BuildRequires: txt2man
BuildRequires: bgscripts-core
Buildarch:	noarch

%description
Myautomount uses the power of autofs to present a system tray icon
that lists mountable removable media.

%prep
%setup -q -c %{name}

%build
export srcdir="$( find . -type d -name 'src' -printf '%%P' )"
%make_build -C "${srcdir}"

%install
export srcdir="$( find . -type d -name 'src' -printf '%%P' )"
%make_install -C "${srcdir}"
MYA_SKIP_RESTART=1 DESTDIR=%{buildroot} %{__make} -C "${srcdir}" initialize
exit 0

%clean
rm -rf %{buildroot}

%preun

%postun

%if 0%{?fedora} || 0%{?rhel} >= 7
%posttrans
update-desktop-database 1>/dev/null 2>&1 || :
%else
%post
update-desktop-database 1>/dev/null 2>&1 || :
%endif

%files
%config %attr(0644, -, -) %{_sysconfdir}/%{name}.conf
%{_sysconfdir}/xdg/autostart
%config %attr(0644, -, -) %{_sysconfdir}/autofs.%{name}
%config %attr(0644, -, -) %{_sysconfdir}/auto.master.d/*
%{_bindir}/*
%{_sysconfdir}/sudoers.d/*
%{_datadir}/applications/*
%{_libexecdir}/%{name}/*
%doc %{_mandir}/man1/*
%if 0%{?fedora} || 0%{?rhel} >= 8
%doc %{_pkgdocdir}/*
%else
%doc %{_docdir}/%{name}/*
%endif

%changelog
* Mon Nov 06 2023 B. Stack <bgstack15@gmail.com> - 0.0.5-1
- New version

* Sat Oct 14 2023 B. Stack <bgstack15@gmail.com> - 0.0.4-1
- New version

* Wed Oct 04 2023 B. Stack <bgstack15@gmail.com> - 0.0.3-1
- New version

* Mon Oct 25 2021 B. Stack <bgstack15@gmail.com> - 0.0.2-1
- New version

* Tue Sep 29 2020 B Stack <bgstack15@gmail.com> - 0.0.1-1
- Initial package build
