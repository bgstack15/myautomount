# README for myautomount
## Overview
Myautomount is a solution for desktop users who want to have easy access to removable media, without dbus. Dependencies include autofs, sudo, python3, and gtk3. This package provides a system tray icon that displays the available removable media that can be mounted with autofs.

Myautomount is my response to the Go tool by Project Trident, [trident-automount][1]. I translated the Go tool almost line-for-line into shell, and then modified it slightly. 

## Design and architecture
A shell script, myautomountd, wraps around `udevadm monitor` to watch for block devices that are added and removed. Upon finding a new block device, the daemon will generate pseudo-xdg [desktop files][2] in a particular directory which defaults to `/run/user/${UID}/media`. At startup, the daemon will insert the current user into the autofs rule so that the files on mounted filesystems will be owned by this user, if applicable.

The tray icon uses a python implementation of [inotify][3] to watch that directory, and generate menus for the left- and right-click actions. The icon supports using the [XApp library][4] for StatusIcon instead of just the [deprecated][5] Gtk.StatusIcon, but it is a setting in the [python script](src/usr/libexec/myautomount/myautomount-trayicon.py) itself and not yet in the Makefile.

## Testing and building myautomount
Build dependencies include txt2man, and my txtman-wrapper from [bgscripts-core][5].
Use environment variable `MYA_PREFIX` for testing.

    export MYA_PREFIX=~/dev/myautomount/src
    sudo DEBUG=1 MYA_PREFIX=~/dev/myautomount/src sh -x usr/libexec/myautomount/myautomount-initialize.sh

## Improvements
* Add notifications, perhaps with [xnotify](https://github.com/phillbush/xnotify)?
* Implement the feature to hide sdb and sdc, etc., because one does not normally place filesystems on the raw drive.
* add links to obs and copr packages

# References
[1]: https://github.com/project-trident/trident-utilities/blame/master/src-go/automount/main.go
[2]: https://specifications.freedesktop.org/desktop-entry-spec/desktop-entry-spec-latest.html
[3]: https://github.com/seb-m/pyinotify
[4]: https://github.com/linuxmint/xapp
[5]: https://developer.gnome.org/gtk3/stable/GtkStatusIcon.html
[6]: https://gitlab.com/bgstack15/bgscripts/-/blob/master/src/usr/bin/txt2man-wrapper
