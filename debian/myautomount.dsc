Format: 3.0 (quilt)
Source: myautomount
Binary: myautomount
Architecture: all
Version: 0.0.5-1
Maintainer: B. Stack <bgstack15@gmail.com>
Homepage: https://bgstack15.wordpress.com/
Standards-Version: 4.5.0
Build-Depends: debhelper-compat (= 12), bgscripts-core, txt2man
Package-List:
 myautomount deb utils optional arch=all
Files:
 00000000000000000000000000000000 1 myautomount.orig.tar.gz
 00000000000000000000000000000000 1 myautomount.debian.tar.xz
