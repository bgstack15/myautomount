# File: Makefile for myautomount
# Location: myautomount source package
# Author: bgstack15
# Startdate: 2020-09-28
# Title: Makefile for myautomount source package
# Purpose: To use traditional Unix make utility
# History:
# Usage:
# Reference:
#    bgscripts Makefile
# Improve:
# Document:
# Dependencies:
#    build-devuan: txt2man, bgscripts-core

APPNAME    = myautomount
APPVERSION = 0.0.5
SRCDIR     = $(CURDIR)
prefix     = /usr
SYSCONFDIR = $(DESTDIR)/etc
DEFAULTDIR = $(DESTDIR)/etc/sysconfig # for debian use '$(DESTDIR)/etc/default'
BINDIR     = $(DESTDIR)$(prefix)/bin
LIBEXECDIR = $(DESTDIR)$(prefix)/libexec
SHAREDIR   = $(DESTDIR)$(prefix)/share
DOCDIR     = $(SHAREDIR)/doc/$(APPNAME)
APPDIR     = $(SHAREDIR)/$(APPNAME)
APPSDIR    = $(SHAREDIR)/applications
MANDIR     = $(SHAREDIR)/man
XDGAUTODIR = $(SYSCONFDIR)/xdg/autostart

# variables for deplist
DEPTYPE = dep
SEPARATOR = ,

awkbin     :=$(shell which awk)
chmodbin   :=$(shell which chmod)
cpbin      :=$(shell which cp)
echobin    :=$(shell which echo)
falsebin   :=$(shell which false)
findbin    :=$(shell which find)
grepbin    :=$(shell which grep)
gzipbin    :=$(shell which gzip)
installbin :=$(shell which install)
lnbin		  :=$(shell which ln)
rmbin      :=$(shell which rm)
rmdirbin   :=$(shell which rmdir)
sedbin     :=$(shell which sed)
sortbin    :=$(shell which sort)
truebin    :=$(shell which true)
txt2manwrapper :=$(shell which txt2man-wrapper)
uniqbin    :=$(shell which uniq)
xargsbin   :=$(shell which xargs)

with_man ?= YES

APPSDIR_SHORT := $(shell ${echobin} "${APPSDIR}" | ${sedbin} -r -e "s:$(DESTDIR)/?::" )

all: build_man

ifeq ($(with_man),YES)
install: build_man install_files
else
install: install_files
endif

.PHONY: clean install install_files build_man uninstall list deplist deplist_opts initialize

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | ${awkbin} -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | ${sortbin} | ${grepbin} -E -v -e '^[^[:alnum:]]' -e '^$@$$'

deplist:
	@# deplist 2020-04-18 input must be comma separated
	@# DEPTYPE( dep , rec , sug ) for depends, recommends, or suggests
	@if test -z "${DISTRO}" ; then ${echobin} "Please run \`make deplist\` with DISTRO= one of: `make deplist_opts 2>&1 1>/dev/null | ${xargsbin}`. Aborted." 1>&2 ; exit 1 ; fi
	@if ! ${echobin} "${DEPTYPE}" | grep -qE "^(dep|rec|sug)$$" ; then ${echobin} "Please run \`make deplist\` with DEPTYPE= one of: dep, rec, sug. Undefined will use \`dep\`. Aborted." 1>&2 ; exit 1; fi
	@${grepbin} -h --exclude-dir='doc' -riIE "\<${DEPTYPE}-" ${SRCDIR} | ${awkbin} -v "domain=${DISTRO}" -v "deptype=${DEPTYPE}" 'tolower($$2) ~ deptype"-"domain {$$1="";$$2="";print}' | tr ',' '\n' | ${sortbin} | ${uniqbin} | ${sedbin} -r -e 's/^\s*//' -e "s/\s*\$$/${SEPARATOR}/" | ${xargsbin}

deplist_opts:
	@# deplist_opts 2020-04-18 find all available dependency domains
	@${grepbin} -h -o -riIE '\<(dep|rec|sug)-[^\ :]+:' ${SRCDIR} | ${sedbin} -r -e 's/(dep|rec|sug)-//;' -e 's/:$$//;' | ${sortbin} | ${uniqbin} 1>&2

install_files:
	@ls usr/share/man/man*/*gz 1>/dev/null 2>&1 && echo "Including man pages." || :
	@${echobin} Installing files to ${DESTDIR}
	for td in $$( ${findbin} ${SRCDIR} -type d ! -name '.*.swp' ! -name 'Makefile' -printf '%P\n' | ${sedbin} -r -e "s:etc/sysconfig:${DEFAULTDIR}:" -e "s:${DESTDIR}/?::" ) ; do ${installbin} -m0755 -d ${DESTDIR}/$${td} ; done
	for tf in $$( ${findbin} ${SRCDIR} ! -type d ! -type l ! -name '.*.swp' ! -name 'Makefile' ! \( -path '*/man/*' -name '*.txt' \) ! -path '*/sysconfig/*' -printf '%P\n' ) ; do MODE=0644 ; echo "$${tf}" | grep -qE "(bin|libexec|deprecated)\/" && MODE=0755 ; ${installbin} -m$${MODE} ${SRCDIR}/$${tf} ${DESTDIR}/$${tf} ; done
	${lnbin} -sf ../../../${APPSDIR_SHORT}/myautomountd.desktop ${XDGAUTODIR}/myautomountd.desktop
	${lnbin} -sf ../../../${APPSDIR_SHORT}/myautomount-trayicon.desktop ${XDGAUTODIR}/myautomount-trayicon.desktop

MAN_TXT:=$(wildcard usr/share/man/man*/*.txt)
MAN_GZ:= $(subst .txt,.gz,$(MAN_TXT))

build_man: $(MAN_GZ)

$(MAN_GZ): %.gz: %.txt
	${txt2manwrapper} - < $< | ${gzipbin} > $@

uninstall:
	@${echobin} SRCDIR=${SRCDIR}
	${rmbin} -f $$( ${findbin} ${SRCDIR} -mindepth 1 ! -type d -printf '%p\n' | ${sedbin} -r -e "s:/etc/sysconfig:${DEFAULTDIR}:" -e "s:^${SRCDIR}:${DESTDIR}:" -e "s:${DESTDIR}${DESTDIR}:${DESTDIR}:" )

	# now remove the files laid down from the initialize target.
	${rmbin} -f $(SYSCONFDIR)/autofs.${APPNAME} $(SYSCONFDIR)/auto.master.d/${APPNAME}.autofs

	# remove all installed directories that are now blank.
	for word in $$( ${findbin} ${SRCDIR} -mindepth 1 -type d -printf '%p\n' | ${sedbin} -r -e "s:^${SRCDIR}:${DESTDIR}:" | ${awkbin} '{ print length, $$0 }' | ${sortbin} -rn | ${awkbin} '{print $$2}' ) ; do ${findbin} $${word} -mindepth 1 1>/dev/null 2>&1 | read 1>/dev/null 2>&1 || { ${rmdirbin} "$${word}" 2>/dev/null || ${truebin} ; } ; done

# custom section for myautomount
initialize:
	@echo "Must run this after \`install\`"
	export MYA_PREFIX=$(DESTDIR) ; \
		sh -x $(DESTDIR)/usr/libexec/myautomount/myautomount-initialize.sh

clean:
	-@#${echobin} "target $@ not implemented yet! Gotta say unh." && ${falsebin}
	-${rmbin} -f usr/share/man/man*/*.gz || :
