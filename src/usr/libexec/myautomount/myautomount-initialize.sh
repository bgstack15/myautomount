#!/bin/sh
# SPDX-License-Identifier: BSD-2-Clause
# The daemon will call this once upon starting.
# MYA_SKIP_RESTART is for when you are building a package. Set it to any value, to skip the `service autofs restart`.

fail() {
   echo "${@}" 1>&2
   exit 1
}

setupSystem() {
   _needrestart=0
   mkdir -m0755 -p "${MYA_PREFIX}${AUTOMOUNT_DIR}" || fail "Could not setup autofs rules! Check if this is being run as root?"
   ! grep -q -e "uid=${AUTOMOUNT_USER}," "${MYA_PREFIX}${AUTOMOUNT_FILE}.fat32" 2>/dev/null && {
      {
         touch "${MYA_PREFIX}${AUTOMOUNT_FILE}"
         echo "# for fat32, ntfs"
         echo "* -fstype=auto,noatime,rw,nosuid,uid=${AUTOMOUNT_USER},gid=users  :/dev/& "
      } > "${MYA_PREFIX}${AUTOMOUNT_FILE}.fat32" || fail "Could not setup autofs rules! Check if this is being run as root?"
      _needrestart=1
   }
   ! grep -q -e "fstype=auto,noatime" "${MYA_PREFIX}${AUTOMOUNT_FILE}.ext4" 2>/dev/null && {
      {
         echo "# for ext4"
         echo "* -fstype=auto,noatime,rw,nosuid  :/dev/& "
      } > "${MYA_PREFIX}${AUTOMOUNT_FILE}.ext4" || fail "Could not setup autofs rules! Check if this is being run as root?"
      _needrestart=1
   }
   ! grep -q -e "fstype=auto,noatime" "${MYA_PREFIX}${AUTOMOUNT_FILE}" 2>/dev/null && {
      {
         echo "# for CDs"
         echo "* -fstype=auto,noatime,rw,nosuid  :/dev/& "
      } > "${MYA_PREFIX}${AUTOMOUNT_FILE}" || fail "Could not setup autofs rules! Check if this is being run as root?"
      _needrestart=1
   }
   ! grep -q -e "${AUTOMOUNT_BROWSEDIR}" "${MYA_PREFIX}${AUTOMOUNT_DIR_FILE}" 2>/dev/null && {
      {
         touch "${MYA_PREFIX}${AUTOMOUNT_DIR_FILE}"
         echo "${AUTOMOUNT_BROWSEDIR}/ext4   ${AUTOMOUNT_FILE}.ext4       --timeout=5 "
         echo "${AUTOMOUNT_BROWSEDIR}/fat32  ${AUTOMOUNT_FILE}.fat32      --timeout=5 "
         echo "${AUTOMOUNT_BROWSEDIR}/ntfs   ${AUTOMOUNT_FILE}.fat32      --timeout=5 "
         echo "${AUTOMOUNT_BROWSEDIR}        ${AUTOMOUNT_FILE}            --timeout=5 "
      } > "${MYA_PREFIX}${AUTOMOUNT_DIR_FILE}" || fail "Could not setup autofs rules! Check if this is being run as root?"
      _needrestart=1
   }
   test ${_needrestart} -eq 1 && test -z "${MYA_SKIP_RESTART}" && eval "service autofs restart"
}

export AUTOMOUNT_DAEMON=1 # to suppress the useless error about mktemp: failed to create file
. ${MYA_PREFIX}/etc/myautomount.conf
setupSystem
# must enforce exit 0 because otherwise an already-initialized system will end with test ${_needrestart} -eq 1 as false which will break `make`.
:
